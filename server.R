function(input, output, session) {
  # Changing color of navbar
  observe({
    if (!is.null(input$selected_tab))
      if (input$selected_tab == "overview") {
        addClass(selector = "body", class = "skin-yellow")
        removeClass(selector = "body", class = "skin-red")
        removeClass(selector = "body", class = "skin-green")
        removeClass(selector = "body", class = "skin-blue")
      }
    else if (input$selected_tab == "descriptive") {
      addClass(selector = "body", class = "skin-red")
      removeClass(selector = "body", class = "skin-green")
      removeClass(selector = "body", class = "skin-blue")
      removeClass(selector = "body", class = "skin-yellow")
    } else if (input$selected_tab == "eda") {
      addClass(selector = "body", class = "skin-green")
      removeClass(selector = "body", class = "skin-red")
      removeClass(selector = "body", class = "skin-blue")
      removeClass(selector = "body", class = "skin-yellow")
    } else {
      toggleClass(selector = "body", class = "skin-blue")
      removeClass(selector = "body", class = "skin-red")
      removeClass(selector = "body", class = "skin-green")
      removeClass(selector = "body", class = "skin-yellow")
      
    }
  })
  
  
  # Sidebar
  output$overview <- renderMenu({
    sidebarMenu(menuItem(
      "Overview",
      icon = icon("calendar"),
      tabName = "overview"
    ))
  })
  output$descriptive <- renderMenu({
    sidebarMenu(menuItem(
      "Descriptive Statistics",
      icon = icon("chart-area"),
      tabName = "descriptive"
    ))
  })
  output$eda <- renderMenu({
    sidebarMenu(menuItem("EDA",
                         icon = icon("chart-bar")
                         ,
                         tabName = "eda"))
  })
  output$forecast <- renderMenu({
    sidebarMenu(menuItem(
      "Forecast",
      icon = icon("list-alt"),
      tabName = "forecast"
    ))
  })
  
  
  output$overview_introduction <- renderUI({
    str0 <- h1(paste("Dashboard overview"))
    str1 <-
      h4(
        paste(
          "The dashboard contains next to the overview 3 sides. The descriptive statistics side,
          the EDA side and the forecast side. Each side can be reached by the menu on the left."
        )
      )
    str1.5 <- paste("")
    str2 <- h2(paste("The Descriptive statistics side:"))
    str3 <-
      h4(
        paste(
          "The goal of the side is to give a first overview of the dataset,
          before filtering it on the next pages.
          The page starts with multiple KPI Boxes, which display the main findings of the dataset.
          Afterwards the user finds various plots, visualizing the dataset from different kind of views.
          To make it more readable each title includes the type of plot,
          making it also easier to distinguish between categorical and numerical data.
          Furthermore each plot has a text box at its bottom to give an interpretation
          of its result and set the visualization in business context."
        )
      )
    str3.5 <- paste("")
    str4 <- h2(paste("The EDA side:"))
    str5 <-
      h4(
        paste(
          "This page builds uppon the basic understanding of the dataset
          from the descriptive statistics side."
        )
      )
    str6 <-
      h4(
        paste(
          "This page applies advanced behavior and uses advanced Shiny filtering functionality.
          The user is able to filter the dataset on various criteria like country, year, team.
          The output are many different filtered graphs,
          which give the user an even more detailed picture of the football transfer situation."
        )
      )
    str6.5 <- paste("")
    str7 <- h2(paste("Forecast:"))
    str8 <-
      h4(
        paste(
          "This page allows a peak in the future.Unlike the other two pages,
          which focus on historical data, it takes the historical data and predicts transfers.
          The user has again the opportunity to filter on multiple criteria like team and
          calculation method, country and the number of transfers."
        )
      )
    str8.5 <- paste("")
    str9 <- h2(paste("The Dataset / Technical information"))
    str10 <-
      h4(paste(
        "Dataset: https://github.com/d2ski/football-transfers-data"
      ))
    str11 <-
      h4(
        paste(
          "The file contains information about the football transfers between various leagues.
          To make it more useable we filtered and cleaned it.
          We filtered the transfer fee amount column on NaN values.
          Most of the times the reason for NaN values, was that younger players of the same
          club have been transfered to the adult league.
          We also cleaned it on type errors, for example in the age field.
          Furthermore, to make it more readable we clustered the positions to the
          4 main positions like defence, attack, goalkeeper and midfield.
          Lastly we only looked at the incoming transfers and where the transfer was not
          to retirement as well as the season newer than 2014."
        )
      )
    
    HTML(
      paste(
        str0,
        str1,
        str1.5,
        str2,
        str3,
        str3.5,
        str4,
        str5,
        str6,
        str6.5,
        str7,
        str8,
        str8.5,
        str9,
        str10,
        str11,
        sep = '<br/>'
      )
    )
  })
  
  # Descriptive -----------------------------------------
  
  # value boxes
  output$teams <- renderValueBox({
    valueBox(
      paste(dt.football.transfers[, uniqueN(team_id)], "Teams"),
      paste("From", dt.football.transfers[, uniqueN(team_country)], "different leagues"),
      icon = icon("users"),
      color = box.color.descriptive
    )
  })
  output$transfers <- renderValueBox({
    valueBox(
      paste(
        format(dt.football.transfers[, uniqueN(transfer_id)], big.mark = ","),
        "Transfers"
      ),
      paste(
        "With an average of",
        format(
          round(dt.football.transfers[, uniqueN(transfer_id)] /
                  dt.football.transfers[, .N, by = season][, .N], 0),
          big.mark = ','
        ),
        "transfers per year"
      ),
      icon = icon("futbol"),
      color = box.color.descriptive
    )
  })
  output$transfer_money <- renderValueBox({
    valueBox(
      paste(
        format(
          dt.football.transfers[, sum(transfer_fee_amnt, na.rm = TRUE)],
          nsmall = 2,
          big.mark = ","
        ),
        "EUR"
      ),
      paste("Over a course of", dt.football.transfers[, uniqueN(season)], "Seasons"),
      icon = icon("euro-sign"),
      color = box.color.descriptive
    )
  })
  output$mean_age <- renderValueBox({
    valueBox(
      paste(round(dt.football.transfers[, mean(player_age, na.rm = TRUE)], 2)),
      paste("Is the average age of the transferred players"),
      icon = icon("hashtag"),
      color = box.color.descriptive
    )
  })
  output$nations <- renderValueBox({
    valueBox(
      paste(dt.football.transfers[, uniqueN(player_nation)], "Nationalities"),
      paste(
        dt.football.transfers[, .N, by = player_nation][order(-N)][[1:1]][1],
        "is the most common one, with",
        dt.football.transfers[, .N, by = player_nation][order(-N)][[2:1]],
        "players"
      ),
      icon = icon("globe"),
      color = box.color.descriptive
    )
  })
  output$position <- renderValueBox({
    valueBox(
      paste(dt.football.transfers[, .N, by = player_pos][order(-N)][[1:1]][1]),
      paste("Is the most common transferred player position"),
      icon = icon("crosshairs"),
      color = box.color.descriptive
    )
  })
  output$teamcountry <- renderValueBox({
    valueBox(
      paste(dt.football.transfers[, .N, by = team_country][order(-N)][[1:1]][1]),
      paste("Is the most frequent team country"),
      icon = icon("plane-arrival"),
      color = box.color.descriptive
    )
  })
  output$teamcountercountry <- renderValueBox({
    valueBox(
      paste(dt.football.transfers[, .N, by = counter_team_country][order(-N)][[1:1]][1]),
      paste("Is the most frequent counter team country"),
      icon = icon("plane-departure"),
      color = box.color.descriptive
    )
  })
  output$mosttransferred <- renderValueBox({
    valueBox(
      paste(dt.football.transfers[, .N, by = player_name][order(-N)][1][[1]]),
      paste(
        "Is the most transferred player, having being transferred",
        dt.football.transfers[, .N, by = player_name][order(-N)][1][[2]],
        "times"
      ),
      icon = icon("glyphicon glyphicon-refresh", lib = "glyphicon"),
      color = box.color.descriptive
    )
  })
  output$mostexpensive <- renderValueBox({
    valueBox(
      paste(dt.football.transfers[order(-transfer_fee_amnt)][[8]][[1]]),
      paste(
        "Was the most expensive transfer in the world, having cost",
        format(
          dt.football.transfers[, max(transfer_fee_amnt)],
          scientific = FALSE,
          nsmall = 2,
          big.mark = ","
        ) ,
        "EUR"
      ),
      icon = icon("money-bill"),
      color = box.color.descriptive
    )
  })
  output$mostspender <- renderValueBox({
    valueBox(
      paste(dt.football.transfers[, sum(transfer_fee_amnt, na.rm = TRUE),
                                  by = team_name][order(-V1)][[1:1]][1]),
      paste(
        "Is the most spender team having spent a total of",
        format(
          dt.football.transfers[, sum(transfer_fee_amnt, na.rm = TRUE),
                                by = team_name][order(-V1)][[2:1]],
          nsmall = 2,
          big.mark = ","
        ) ,
        "EUR"
      ),
      icon = icon("glyphicon glyphicon-shopping-cart", lib = "glyphicon"),
      color = box.color.descriptive
    )
  })
  output$mostgainer <- renderValueBox({
    valueBox(
      paste(dt.football.transfers[, sum(transfer_fee_amnt, na.rm = TRUE),
                                  by = counter_team_name][order(-V1)][[1:1]][1]),
      paste(
        "Is the most gainer team having received a total of",
        format(
          dt.football.transfers[, sum(transfer_fee_amnt, na.rm = TRUE),
                                by = counter_team_name][order(-V1)][[2:1]],
          nsmall = 2,
          big.mark = ","
        ) ,
        "EUR"
      ),
      icon = icon("piggy-bank"),
      color = box.color.descriptive
    )
  })
  
  ###### Plots ######
  output$transfer_age <- renderPlot({
    ggplot(dt.football.transfers, aes(player_age)) +
      geom_histogram(fill = "darkred", binwidth = 1) +
      geom_vline(
        xintercept = dt.football.transfers[, mean(player_age, na.rm = TRUE)],
        linetype = "dashed",
        color = "gold",
        size = 1
      ) +
      stat_bin(geom = "text",
               aes(label = ..count..),
               vjust = -0.5) +
      labs(x = "Age of Player", y = "Amount of Transfers")
  })
  
  output$text_g1 <- renderText({
    paste(
      "By looking at the histogram of the ages of the transferred players
      we can observe a positive skewed distribution.
      The most common age is 22 years old, which is below the average (23.72)."
    )
  })
  
  output$transfer_country <- renderPlot({
    ggplot(dt.football.transfers[, .N, by = team_country],
           aes(x = team_country, y = N)) +
      geom_bar(stat = "identity", fill = "darkred") +
      geom_text(aes(label = N), vjust = -0.75) +
      xlab("Country") +
      ylab("Amount of Transfers")
  })
  
  output$text_g2 <- renderText({
    paste(
      "By looking at the graph above we can see that Portugal and Germany
      are the countries where there are most and least players
      being transferred to, respectively"
    )
  })
  
  output$transfer_yearly <- renderPlot({
    ggplot(
      dt.football.transfers[, .N, by = list(season, team_country)],
      aes(
        x = season,
        y = N,
        group = team_country,
        color = team_country,
        label = N
      )
    ) +
      geom_line() +
      labs(x = "Season", y = "Amount of Transfers", color = "Team's Country")
  })
  
  output$text_g3 <- renderText({
    paste(
      "By looking at the graph above we can see that from 2014 until 2016,
      England, Italy and Netherlands decreased its number of signignings
      contrary to the rest of the countries. Afterwards,
      the amount of transfer per country keeps pretty stable until 2019
      where all countries experienced a reduction in the number of signigns."
    )
  })
  
  output$transfer_position <- renderPlot({
    positions <- dt.football.transfers[, .N, by = player_pos][[1]]
    x <- dt.football.transfers[, .N, by = player_pos][[2]]
    data.pie <- data.frame(group = positions, value = x)
    y <- round(100 * x / sum(x), 2)
    data.pie$percentage <- y
    ggplot(data.pie, aes(x = "", y = value, fill = positions)) +
      geom_bar(stat = "identity",
               width = 1,
               color = "white") +
      coord_polar("y", start = 0) +
      theme_void() +
      geom_text(aes(label = paste(percentage, "%")),
                size = 5,
                position = position_stack(vjust = 0.5)) +
      labs(fill = "Player Position")
  })
  
  output$text_g4 <- renderText({
    paste(
      "By looking at the graph above we can conclude that the most
      transferred players are attackers, almost accounting forone third of all transfers.
      After comes the defenders followed by the midfielders. Lastly, and naturally,
      comes the goalkeepers representing less than 10% of the total transfers."
    )
  })
  
  output$g_plot <- renderPlot({
    plot(
      g.transfers ,
      vertex.size = V(g.transfers)$vertex.size,
      vertex.label = NA,
      vertex.color = V(g.transfers)$color,
      edge.width = 0.01
    )
    legend("bottomleft",
           legend = levels(as.factor(V(g.transfers)$league)),
           fill = c(levels(as.factor(
             V(g.transfers)$color
           ))))
  })
  
  output$text_g5 <- renderText({
    paste(
      "The network provides great economic value as we can see the transfers of the different clubs,
    colored in the league/country they play in.
    By looking at the network, we can see many connections between the different teams,
    but mostly the network shows that the teams from the same league/country form hubs
    as they tend to transfer mostly among themselves.
          In addition, geography seems to play an important role as well,
          as Portuguese clubs transfer mostly with Spanish clubs and German clubs mostly with Dutch clubs.
          This provides great economic value as we can analyze the see that "
    )
  })
  
  output$histofee <- renderPlot({
    ggplot(dt.football.transfers, aes(x = transfer_fee_amnt)) +
      geom_histogram(fill = "darkred", bins = 100) +
      geom_vline(
        xintercept = dt.football.transfers[, mean(transfer_fee_amnt)],
        linetype = "dashed",
        color = "gold",
        size = 1
      ) +
      xlim(0, 100000000) +
      ylim(0, 1500) +
      xlab("Transfer Fee") +
      ylab("Amount of transfers")
  })
  
  output$text_g6 <- renderText({
    paste(
      "By looking at the graph above we can observe that logically
      most players transferred are cheap. This also comes to show that
      really few players are astronomically expensive."
    )
  })
  
  output$violinboxes <- renderPlot({
    ggplot(dt.football.transfers,
           aes(x = league,  y = transfer_fee_amnt, fill = league)) +
      geom_violin() + scale_y_log10() +
      geom_boxplot(width = 0.1, fill = "beige") +
      labs(fill = "Leagues", x = "Leagues", y = "Transfer Fee")
  })
  
  output$text_g7 <- renderText({
    paste(
      "By looking at the graph above we can conclude that England is the country
      where they pay the most, on average, per player. On the other hand,
      it is in Portugal and the Nederlands where players tend to have a lower cost."
    )
  })
  
  output$scatter <- renderPlot({
    ggplot(
      dt.football.transfers,
      aes(
        x = transfer_fee_amnt,
        y = player_age,
        color = league,
        size = transfer_fee_amnt
      )
    ) +
      geom_point(alpha = 0.5) +
      labs(
        size = "Transfer Fee",
        color = "Leagues",
        x = "Transfer Fee",
        y = "Player Age"
      )
  })
  
  output$text_g8 <- renderText({
    paste(
      "By looking at the graph above we can see that the 2 major transfers
      happened in France (Neymar and Mbappe), followed by some players of the Spannish league.
      We can also see Portugal and the Neederlands again with the lowest average costs
      for players and England having a high average but few outliers."
    )
  })
  
  output$boxplot2 <- renderPlot({
    ggplot(dt.football.transfers,
           aes(x = player_pos, y = transfer_fee_amnt)) +
      geom_boxplot(color = "darkred", fill = "beige") + scale_y_log10() +
      labs(x = "Player Position", y = "Transfer Fee")
  })
  
  output$text_g9 <- renderText({
    paste(
      "By looking at the graph above we can see that transfer fee per the player positions
      follow the expected pattern. Goalkeepers are cheaper, on average,
      than the others while attackers and midfielders are, on average, the most expensive ones."
    )
  })
  
  # EDA -----------------------------------------
  output$country_selector = renderUI({
    selectInput(
      "country",
      label = "Country",
      choices = as.character(unique(
        dt.football.transfers[order(team_country),]$team_country
      )),
      selected = NULL
    )
  })
  
  output$team_selector = renderUI({
    dt.selected.country <-
      dt.football.transfers[team_country %in% input$country,][order(team_name),]
    selectInput(
      inputId = "team",
      label = "Team",
      choices = unique(dt.selected.country$team_name),
      selected = 1
    )
  })
  
  output$season_range = renderUI({
    req(input$team)
    dt.selected.club <-
      dt.football.transfers[team_name == input$team,]
    sliderInput(
      "season",
      label = "Since",
      min = min(dt.selected.club$season, na.rm = T),
      max = max(dt.selected.club$season, na.rm = T),
      value = min(dt.selected.club$season, na.rm = T),
      step = 1,
      round = TRUE,
      sep = ""
    )
  })
  
  output$amount_range = renderUI({
    req(input$team)
    dt.selected.club <-
      dt.football.transfers[team_name == input$team, ]
    sliderInput(
      "amount",
      label = "Minimum amount",
      min = min(dt.selected.club$transfer_fee_amnt,
                na.rm = T),
      max = max(dt.selected.club$transfer_fee_amnt,
                na.rm = T),
      value = min(dt.selected.club$transfer_fee_amnt,
                  na.rm = T)
    )
  })
  #info text
  output$input_info = renderText({
    paste(
      "Here you must choose the country, the club, from wichi season onwards
      and the deal value you want to analyze."
    )
  })
  
  #n_transfers
  output$n_transfers <- renderPlot({
    dt.football.transfers <-
      dt.football.transfers[season >= input$season &
                              transfer_fee_amnt >= input$amount,]
    ggplot(dt.football.transfers[team_name == input$team, .N, by = season],
           aes(x = season, y = N)) +
      geom_bar(stat = "identity", fill = "darkgreen") +
      geom_text(aes(label = N), vjust = -0.75) +
      xlab("Season") +
      ylab("Transfers")
  })
  
  #transfers text
  output$singings_info = renderText({
    dt.selected.club <-
      dt.football.transfers[team_name %in% input$team,]
    paste(
      "Number of incoming signings between ",
      input$amount,
      " and ",
      max(dt.selected.club$transfer_fee_amnt, na.rm = T) ,
      " EUR since ",
      input$season,
      "done by ",
      input$team
    )
  })
  
  
  #money
  output$money_spent <- renderPlot({
    req(input$season)
    req(input$team)
    req(input$amount)
    
    dt.football.transfers <-
      dt.football.transfers[season >= input$season &
                              transfer_fee_amnt >= input$amount,]
    ggplot(
      aggregate(transfer_fee_amnt ~ season, dt.football.transfers[team_name %in%
                                                                    input$team, ], sum),
      aes(x = season, y = transfer_fee_amnt)
    ) +
      geom_bar(stat = "identity", fill = "darkgreen") +
      geom_text(aes(label = transfer_fee_amnt), vjust = -0.75) +
      xlab("Season") +
      ylab("Amount Spent")
  })
  
  #money text
  output$money_info = renderText({
    paste(
      "Amount of money spent on transfers starting from ",
      input$amount,
      " EUR, per season, since ",
      input$season,
      " by ",
      input$team
    )
  })
  
  #input to country connector
  output$origin_country_selector = renderUI({
    dt.football.transfers <-
      dt.football.transfers[season >= input$season &
                              transfer_fee_amnt >= input$amount,]
    selectInput(
      "origin_country",
      label = "Country of player's previous teams",
      choices = as.character(unique(
        dt.football.transfers[order(team_country),]$counter_team_country
      )),
      selected = NULL
    )
  })
  
  #label yes or no
  output$label_yes = renderUI({
    checkboxInput("label_activator", label = "Labeled Nodes")
    
  })
  
  
  output$network_info = renderText({
    dt.selected.club <-
      dt.football.transfers[team_name %in% input$team,]
    paste(
      "In this section a network dependent on the input can be explored.
      In this section we analyse transfer relations between clubs from",
      input$country,
      " and ",
      input$origin_country,
      "on transfers upwards of",
      max(dt.selected.club$transfer_fee_amnt, na.rm = T) ,
      "EUR since",
      paste0(input$season, "."),
      "If",
      input$team,
      "is part of the network it is represented as the red node, otherwise all clubs would be in yellow.",
      "On the left side you can choose the second country in order to compare
      the network of this country with the country chosen on the top of the page"
    )
  })
  
  #network analysis
  output$select_network <- renderPlot({
    req(input$season)
    req(input$country)
    req(input$origin_country)
    
    dt.football.transfers <-
      dt.football.transfers[season >= input$season &
                              transfer_fee_amnt >= input$amount,]
    dt.football.transfers.country <-
      dt.football.transfers[team_country == input$country &
                              counter_team_country == input$origin_country]
    dt.clubs.country <-
      dt.football.transfers.country[, team_name, counter_team_name]
    no.nulls.country <-
      dt.clubs.country[rowSums(is.na(dt.football.transfers.country)) == 0, ]
    g.clubs.country <-
      graph.data.frame(dt.clubs.country[, c("counter_team_name", "team_name")])
    
    #the team selected in red (if not in the network everyone is yellow) #avoid error in network
    if (input$team %in% V(g.clubs.country)$name == TRUE) {
      V(g.clubs.country)[input$team]$color <-
        "red"
    } else{
      V(g.clubs.country)$color <- "yellow"
    }
    
    if (input$label_activator == FALSE) {
      plot(
        as.undirected(g.clubs.country) ,
        vertex.size = 5,
        vertex.label = NA ,
        edge.width = E(g.clubs.country)$width
      )
    } else{
      plot(
        as.undirected(g.clubs.country) ,
        vertex.size = 5,
        vertex.label = V(g.clubs.country)$name ,
        edge.width = E(g.clubs.country)$width
      )
    }
  })
  
  #stat values
  output$avg_path <- renderUI({
    dt.football.transfers <-
      dt.football.transfers[season >= input$season &
                              transfer_fee_amnt >= input$amount, ]
    dt.football.transfers.country <-
      dt.football.transfers[input$country == team_country &
                              input$origin_country == counter_team_country, ]
    dt.clubs.country <-
      dt.football.transfers.country[, team_name, counter_team_name]
    g.clubs.country <-
      graph.data.frame(dt.clubs.country[, c("team_name", "counter_team_name")])
    paste("Average Path Length: ",
          average.path.length(as.undirected(g.clubs.country)))
  })
  
  output$clustering_coeff <- renderUI({
    dt.football.transfers <-
      dt.football.transfers[season >= input$season &
                              transfer_fee_amnt >= input$amount, ]
    dt.football.transfers.country <-
      dt.football.transfers[input$country == team_country &
                              input$origin_country == counter_team_country, ]
    dt.clubs.country <-
      dt.football.transfers.country[, team_name, counter_team_name]
    g.clubs.country <-
      graph.data.frame(dt.clubs.country[, c("team_name", "counter_team_name")])
    paste(
      "Average Cluestering Coefficient: ",
      transitivity(as.undirected(g.clubs.country), type = "average")
    )
  })
  
  output$diameter <- renderUI({
    dt.football.transfers <-
      dt.football.transfers[season >= input$season &
                              transfer_fee_amnt >= input$amount, ]
    dt.football.transfers.country <-
      dt.football.transfers[input$country == team_country &
                              input$origin_country == counter_team_country, ]
    dt.clubs.country <-
      dt.football.transfers.country[, team_name, counter_team_name]
    g.clubs.country <-
      graph.data.frame(dt.clubs.country[, c("team_name", "counter_team_name")])
    paste("Diameter: ", diameter(as.undirected(g.clubs.country)))
  })
  
  #degree histogram
  output$deg_hist <- renderPlot({
    dt.football.transfers <-
      dt.football.transfers[season >= input$season &
                              transfer_fee_amnt >= input$amount, ]
    dt.football.transfers.country <-
      dt.football.transfers[input$country == team_country &
                              input$origin_country == counter_team_country, ]
    dt.clubs.country <-
      dt.football.transfers.country[, team_name, counter_team_name]
    g.clubs.country <-
      graph.data.frame(dt.clubs.country[, c("team_name", "counter_team_name")])
    ggplot() + geom_histogram(aes(x = degree(as.undirected(
      g.clubs.country
    ))), binwidth = 1, fill = "darkgreen") +
      xlab("Degree") +
      ylab("Count")
    
  })
  
  #top10
  output$top_transfers <- renderDataTable({
    dt.football.transfers <-
      dt.football.transfers[season >= input$season &
                              transfer_fee_amnt >= input$amount, ]
    dt.football.transfers[team_name == input$team, ][
      order(-transfer_fee_amnt)][, c("team_name",
                                     "player_name",
                                     "counter_team_name",
                                     "transfer_fee_amnt",
                                     "season")]
  })
  
  output$network_analysis_description <- renderText({
    paste(
      paste(
        "On the left a histogram with the number of the existing degree levels
        among the network between",
        input$country,
        "and" ,
        input$origin_country,
        "(country of player's previous teams) is displayed."
      ),
      paste(
        "On the right side this network with the respective statistics and
        the possibility to show the names of the clubs is displayed."
      ),
      sep = "\n"
    )
  })
  
  
  
  output$top_transfers_all_time <- renderText({
    paste(
      "Top transfers of",
      input$team,
      "since",
      input$season,
      "with a minimum transfer value of",
      format(input$amount, big.mark = ",") ,
      "EUR"
    )
  })
  
  output$top_signings_info = renderText({
    # Wait for inputs to be loaded
    req(input$team)
    
    dt.selected.club <-
      dt.football.transfers[team_name == input$team,]
    paste(
      "Top signings between ",
      input$amount,
      " and ",
      max(dt.selected.club$transfer_fee_amnt, na.rm = T) ,
      " EUR since ",
      input$season,
      "made by ",
      input$team
    )
  })
  
  
  # Forecast -----------------------------------------
  output$country_selector_pred = renderUI({
    selectInput(
      inputId = "country_pred",
      label = "Country",
      choices = as.character(unique(
        dt.football.transfers[order(team_country), ]$team_country
      )),
      selected = NULL
    )
  })
  
  output$team_selector_pred = renderUI({
    dt.selected.country = dt.football.transfers[input$country_pred ==
                                                  team_country, ][order(team_name), ]
    selectInput(
      inputId = "team_pred",
      label = "Team",
      choices = unique(dt.selected.country$team_name),
      selected = 1
    )
  })
  
  output$calc_type = renderUI({
    selectInput(
      inputId = "calc_type",
      label = "Type of calculation",
      choices = c("Common Neighbors", "Jaccard Index", "Adamic-Adar Index"),
      selected = 1
    )
  })
  
  output$number_of_selections = renderUI({
    sliderInput(inputId = "number_of_selections",
                label = "Select the number of predicted links to show",
                1,
                10,
                5,
                step = 1)
  })
  
  f.forecast <- function() {
    V(g.transfers)$nr <- 1:length(V(g.transfers))
    
    # Wait for inputs to be loaded
    req(input$calc_type)
    req(input$team_pred)
    
    if (input$calc_type == "Common Neighbors") {
      
      # Existent edges will be considered as edges can appear more than once betw. same teams
      m.predicted.edges <- as.matrix(cocitation(g.transfers))
      
      top.team.ids <-
        sort(m.predicted.edges[input$team_pred,],
             index.return = TRUE,
             decreasing = TRUE)$ix[1:input$number_of_selections + 2]
      top.team.scores <-
        sort(m.predicted.edges[input$team_pred,],
             index.return = TRUE,
             decreasing = TRUE)$x[1:input$number_of_selections + 2]
      
      # get Team name by index
      top.team.names <-
        get.vertex.attribute(g.transfers, name = "name", index = top.team.ids)
      
      list(team = top.team.names, score = top.team.scores)
      
    } else if (input$calc_type == "Jaccard Index") {
      prediction.jaccard <- similarity(g.transfers, method = "jaccard")
      team.id <-
        get.vertex.attribute(g.transfers, "nr", input$team_pred)
      simularity.team <- prediction.jaccard[team.id,]
      
      top.team.ids <-
        sort(simularity.team,
             index.return = TRUE,
             decreasing = TRUE)$ix[1:input$number_of_selections + 2]
      top.team.scores <-
        sort(simularity.team,
             index.return = TRUE,
             decreasing = TRUE)$x[1:input$number_of_selections + 2]
      
      # get Team name by index
      top.team.names <-
        get.vertex.attribute(g.transfers, name = "name", index = top.team.ids)
      
      list(team = top.team.names, score = top.team.scores)
      
    } else {
      prediction.invlogweight <-
        similarity(g.transfers, method = "invlogweight")
      team.id <-
        get.vertex.attribute(g.transfers, "nr", input$team_pred)
      simularity.team <- prediction.invlogweight[team.id,]
      
      top.team.ids <-
        sort(simularity.team,
             index.return = TRUE,
             decreasing = TRUE)$ix[1:input$number_of_selections + 2]
      top.team.scores <-
        sort(simularity.team,
             index.return = TRUE,
             decreasing = TRUE)$x[1:input$number_of_selections + 2]
      
      # get Team name by index
      top.team.names <-
        get.vertex.attribute(g.transfers, name = "name", index = top.team.ids)
      
      list(team = top.team.names,
           score = top.team.scores,
           ids = top.team.ids)
    }
  }
  
  output$link_prediction <- renderTable({
    data.table(team = f.forecast()$team, score = f.forecast()$score)
  })
  
  output$link_prediction_title <- renderText({
    paste(
      "Top",
      input$number_of_selections,
      "predicted transfers of",
      input$team_pred,
      "by",
      input$calc_type
    )
  })
  
  #label yes or no
  output$label_yes_pred = renderUI({
    checkboxInput("label_activator_pred", label = "Label Nodes")
    
  })
  
  
  output$link_network <- renderPlot({
    g.transfers.induced <-
      induced.subgraph(g.transfers, V(g.transfers)[append(f.forecast()$team,
                                                          input$team_pred)])
    

    # Color team in red
    V(g.transfers.induced)[name == input$team_pred]$coloring <- "red"
    if (input$label_activator_pred) {
      plot(g.transfers.induced, vertex.color = V(g.transfers.induced)$coloring)
    } else {
      plot(g.transfers.induced, vertex.label = NA,
           vertex.color = V(g.transfers.induced)$coloring)
    }
  })
  
  output$link_network_title <- renderText({
    paste(
      "Existent Network of",
      input$team_pred,
      "and top",
      input$number_of_selections,
      "predicted teams"
    )
  })
  
  output$link_network_description <- renderText({
    paste(
      "The left chart shows the most probable next link between the chosen team
      and the teams displayed already orderd by likelihood.
      On the right side we see the current network of those teams and
      the previous transfers between all of those clubs.
      Our economic value from this analysis is to predict which teams are likely to
      make a deal with each other, how closely they are linked and how this could
      influence the European football ecosystem"
    )
  })
  
}