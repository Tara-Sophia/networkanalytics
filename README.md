# Network Analytics - Analyzing incoming football transfers

## Project overview

This project provides a dashboard which contains next to the overview 3 sides. The descriptive statistics side, the EDA side and the forecast side. Each side can be reached by the menu on the left.


### The Descriptive statistics side

The goal of the side is to give a first overview of the dataset, before filtering it on the next pages. The page starts with multiple KPI Boxes, which display the main findings of the dataset. Afterwards the user finds various plots, visualizing the dataset from different kind of views. To make it more readable each title includes the type of plot, making it also easier to distinguish between categorical and numerical data. Furthermore each plot has a text box at its bottom to give an interpretation of its result and set the visualization in business context.


### The EDA side

This page builds uppon the basic understanding of the dataset from the descriptive statistics side.

This page applies advanced behavior and uses advanced Shiny filtering functionality. The user is able to filter the dataset on various criteria like country, year, team. The output are many different filtered graphs, which give the user an even more detailed picture of the football transfer situation.


### Forecast

This page allows a peak in the future.Unlike the other two pages, which focus on historical data, it takes the historical data and predicts transfers. The user has again the opportunity to filter on multiple criteria like team and calculation method, country and the number of transfers.


### The Dataset / Technical information

Dataset: https://github.com/d2ski/football-transfers-data

The file contains information about the football transfers between various leagues. To make it more useable we filtered and cleaned it. We filtered the transfer fee amount column on NaN values. Most of the times the reason for NaN values, was that younger players of the same club have been transfered to the adult league. We also cleaned it on type errors, for example in the age field. Furthermore, to make it more readable we clustered the positions to the 4 main positions like defence, attack, goalkeeper and midfield. Lastly we only looked at the incoming transfers and where the transfer was not to retirement as well as the season newer than 2014.


## Deployed App
https://network-analytics-football-transfers.shinyapps.io/networkanalytics/


## Authors and acknowledgment
- Diogo Passeiro
- Carlos Lourenco
- Tara Tumbraegel
- Florentin von Haugwitz


## License
This Project is equipped with a MIT License. Please refer to the LICENSE file for further details
